from resources.functions import get_password_len, create_random_password, total_options


def main():
    l = get_password_len()
    p = create_random_password(l)
    c = total_options(l)
    print('Your password is: {}'.format(p))
    print('Possible combinations: {}'.format(c))

if __name__ == "__main__":
    main()